<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'api', 'prefix' => 'v1'], function () {
    Route::prefix('auth')->group(function () {
        Route::post('login', 'AuthController@login')->name('auth.login');
        Route::middleware('auth:airlock')->group(function () {
            Route::get('user', 'AuthController@user')->name('auth.user');
            Route::post('logout', 'AuthController@logout')->name('auth.logout');
        });
    });
    Route::middleware('auth:airlock')->group(function () {
        Route::prefix('currencies')->group(function () {
            Route::get('rates', 'CurrencyController@getCurrenciesRates')->name('currencies.rates');
            Route::get('{currency}/rates', 'CurrencyController@getCurrencyRates')->where('id', '[0-9]+')->name('currency.rates');
        });
    });
});
