<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    public function setUp(): void
    {
        parent::setup();
        DB::beginTransaction();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateUser()
    {
        $attributes = $this->getUserAttributes();
        $this->artisan('user:create')
            ->expectsQuestion('Enter username', $attributes['username'])
            ->expectsQuestion('Enter password', $attributes['password'])
            ->expectsOutput('User has been created successfully!')
            ->assertExitCode(0);
    }

    /**
     * Gets a sample user for testing purposes
    */
    private function getUserAttributes()
    {
        return [
            'username' => 'qwerty',
            'password' => '12345678'
        ];
    }

    public function tearDown(): void
    {
        DB::rollBack();
        parent::tearDown();
    }
}
