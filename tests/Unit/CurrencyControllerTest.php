<?php

namespace Tests\Feature;

use App\Models\Currency;
use App\Models\User;
use Tests\TestCase;

class CurrencyControllerTest extends TestCase
{
    protected User $user;

    public function setUp(): void
    {
        parent::setup();
        $this->user = $this->getUser();
    }

    /**
     * Test get latest currencies rates
     *
     * @return void
     */
    public function testGetCurrenciesRates()
    {
        $response = $this->actingAs($this->user)
            ->get(route('currencies.rates'));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'pagination'
            ]);
    }

    /**
     * Test get currency rates
     *
     * @return void
     */
    public function testGetCurrencyRates()
    {
        $currency = Currency::first();
        $response = $this->actingAs($this->user)
            ->get(route('currency.rates', [ 'currency' => $currency->id ]));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'pagination'
            ]);
    }

    /**
     * Gets a user instance for testing purposes
     */
    private function getUser(): User
    {
        return User::first();
    }
}
