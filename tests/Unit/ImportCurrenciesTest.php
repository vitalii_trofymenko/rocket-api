<?php

namespace Tests\Unit;

use Tests\TestCase;

class ImportCurrenciesTest extends TestCase
{
    /**
     * Test import currencies
     *
     * @return void
     */
    public function testImportCurrencies()
    {
        $this->artisan('currencies:import')
            ->expectsOutput('Imported!')
            ->assertExitCode(0);
    }
}
