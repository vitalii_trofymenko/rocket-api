<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetCurrencyRatesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page_size' => 'sometimes|numeric',
            'page' => 'sometimes|numeric',
            'date_from' => 'sometimes|required_with:date_to|date',
            'date_to' => 'sometimes|required_with:date_from|date'
        ];
    }
}
