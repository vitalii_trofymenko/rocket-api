<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetCurrencyRatesRequest;
use App\Http\Resources\CurrencyRateResourceCollection;
use App\Http\Resources\RateResourceCollection;
use App\Models\Currency;
use App\Services\CurrencyService;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    protected CurrencyService $currencyService;

    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }

    public function getCurrenciesRates(Request $request)
    {
        $currencies = $this->currencyService->latestCurrenciesRates($request);

        return new CurrencyRateResourceCollection($currencies);
    }

    public function getCurrencyRates(GetCurrencyRatesRequest $request, Currency $currency)
    {
        $rates = $this->currencyService->currencyRates($request, $currency);

        return new RateResourceCollection($rates);
    }
}
