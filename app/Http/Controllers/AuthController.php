<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Resources\AuthUserResource;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        if (!Auth::attempt($request->only('username', 'password'))) {
            return response()->json([
                'message' => 'Credentials is invalid'
            ], 401);
        }

        $token = auth()->user()->createToken('spa')->plainTextToken;

        return response()->json([
            'data' => [
                'token' => $token,
                'type' => 'bearer'
            ]
        ], 200);
    }

    public function user()
    {
        return response()->json([
            'data' => new AuthUserResource(auth()->user())
        ], 200);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return response(null, 204);
    }
}
