<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RateResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'pagination' => [
                'from' => $this->firstItem(),
                'to' => $this->lastItem(),
                'total' => $this->total(),
                'page' => $this->currentPage(),
                'lastPage' => $this->lastPage()
            ]
        ];
    }

    public function toResponse($request)
    {
        return JsonResource::toResponse($request);
    }
}
