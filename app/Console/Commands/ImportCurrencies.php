<?php

namespace App\Console\Commands;

use App\Models\Rate;
use Illuminate\Console\Command;

class ImportCurrencies extends Command
{
    private const XML_URL = 'http://www.cbr.ru/scripts/XML_daily.asp';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currencies:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import currencies';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = simplexml_load_file(self::XML_URL);
        $date = date("Y-m-d", strtotime($file->attributes()->Date));

        foreach ($file->Valute as $item) {
            Rate::updateOrCreate(
                ['currency_id' => $item->NumCode, 'date' => $date],
                [
                    'nominal' => $item->Nominal,
                    'value' => floatval(str_replace(",",".", $item->Value))
                ]
            );
        }

        $this->info('Currencies rates have been imported!');
    }
}
