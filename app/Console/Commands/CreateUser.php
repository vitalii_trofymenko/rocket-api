<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $attributes = $this->getAttributes();

        User::create([
            'username' => $attributes['username'],
            'password' => Hash::make($attributes['password'])
        ]);

        $this->info('User has been created successfully!');
    }

    private function getAttributes(): array
    {
        $attributes['username'] = $this->ask('Enter username');
        $attributes['password'] = $this->secret('Enter password');

        if (!$this->validate($attributes)) {
            return $this->getAttributes();
        }

        return $attributes;
    }

    private function validate($attributes): bool
    {
        $validator = Validator::make($attributes, [
            'username' => 'required|unique:users',
            'password' => 'required|min:8'
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return false;
        }

        return true;
    }
}
