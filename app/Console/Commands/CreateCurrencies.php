<?php

namespace App\Console\Commands;

use App\Models\Currency;
use Illuminate\Console\Command;

class CreateCurrencies extends Command
{
    private const XML_URL = 'http://www.cbr.ru/scripts/XML_daily.asp';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currencies:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create currencies';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = simplexml_load_file(self::XML_URL);

        foreach ($file->Valute as $item) {
            Currency::firstOrCreate(['id' => $item->NumCode], [
                'char_code' => $item->CharCode,
                'name' => $item->Name
            ]);
        }

        $this->info('Currencies have been created!');
    }
}
