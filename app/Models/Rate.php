<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    public $guarded = [];
    public $primaryKey = 'currency_id';

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
