<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    public $guarded = [];

    public function rates()
    {
        return $this->hasMany(Rate::class);
    }

    public function latestRate()
    {
        return $this->hasOne(Rate::class)->latest('date');
    }
}
