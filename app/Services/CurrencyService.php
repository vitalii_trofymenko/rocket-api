<?php

namespace App\Services;

use App\Models\Currency;
use Illuminate\Http\Request;

class CurrencyService
{
    public function latestCurrenciesRates(Request $request)
    {
        $take = $request->get('page_size');

        return Currency::with(['latestRate'])->paginate($take);
    }

    public function currencyRates(Request $request, Currency $currency)
    {
        $take = $request->get('page_size');

        $rates = $currency->rates();

        if ($request->has('date_from')) {
            $rates = $rates->whereBetween('date', [$request->get('date_from'), $request->get('date_to')]);
        }

        return $rates->latest('date')->paginate($take);
    }
}
