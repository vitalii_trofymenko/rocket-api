<?php

use Illuminate\Database\Seeder;

class RatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $currencies = \App\Models\Currency::all();
        $date = \Carbon\Carbon::now()->subMonth();

        while (!$date->isSameAs('Y-m-d', \Carbon\Carbon::now())) {
            foreach ($currencies as $currency) {
                $currency->rates()->create([
                    'nominal' => $faker->numberBetween(1, 1000),
                    'value' => $faker->randomFloat(4, 1, 10),
                    'date' => $date->format('Y-m-d')
                ]);
            }
            $date->addDay();
        }
    }
}
