## Currencies rates API

### Installation

#### composer
```bash
composer install
```

### Commands

#### currencies
Create currencies:
```bash
php artisan currencies:create
```
Import latest currencies rates:
```bash
php artisan currencies:import
```

#### user
Create a new user:
```bash
php artisan user:create
```
